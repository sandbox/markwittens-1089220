Theme autoswitch enabled admins to setup automatic switches of the theme of the site.
This can be used to i.e. switch the site to a christmas theme every year around christmas,
switch to a summer theme in the summer or maybe automatically switch the site to
it's brand new designed theme on a pre-selecte release date.

Theme Autoswitch was written by Mark Wittens (markwittens)
- http://nl.linkedin.com/in/markwittens
- http://www.nilsson.nl

Features
------------

* Automatically switches themes based on specified dates

Usage
------------

1. Open "Theme Autoswitch rules" from the menu or go to /admin/settings/theme_autoswitch

2. Click "Add a rule" to add a rule

3. Select a theme and the from/to dates between which this theme should be active

4. Lean back and wait for the theme to change (or look again on the day you selected)

Known issues
-------------

* Themes need to be enabled or they won't be available for switching. This means
  that if Drupal is set up so users can switch themes themselves the themes will
  be available there as well.

* It's only possible to switch on a daily basis for now, the switch will happen
  at midnight of the selected day and will not change back until the end of the selected
  end day.
