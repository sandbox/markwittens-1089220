<?php
// $Id$

/**
 * @file
 *
 * Theme Autoswitch rule administration pages
 *
 * @author Mark Wittens 2010
 *
 */

/**
 * Displays the admin page with the existing rules and a link to add new rules.
 *
 * @return string
 */
function theme_autoswitch_admin() {
  $header = array(t('#'), t('Date from'), t('Date until'), t('Theme'), t('Links'));

  $rules = _theme_autoswitch_getRules();
  foreach ($rules as $rule) {
    $rows[] = array($rule->id, _theme_autoswitch_displayDate($rule->date_from), _theme_autoswitch_displayDate($rule->date_until), $rule->theme, _theme_autoswitch_get_edit_links($rule->id));
  }
  $output  = theme('table', $header, $rows, array());
  $output .= l(t('Add rule'), 'admin/settings/theme_autoswitch/add');

  return $output;
}

/**
 * Build the add/edit form for a rule
 *
 * @param $form_state
 * @param $record
 * @return array
 */
function theme_autoswitch_edit($form_state, $record = NULL) {
  drupal_add_css(drupal_get_path('module', 'theme_autoswitch') . '/stylesheets/theme_autoswitch.css', 'module');

  $form = array();

  $form['id'] = array(
    '#type' => 'hidden',
    '#title' => t('#'),
    '#description' => t('The unique identifier of this rule'),
    '#maxlength' => 10,
    '#default_value' => $record ? $record->id : NULL,
  );

  $form['explain'] = array(
    '#type' => 'markup',
    '#value' => t('<strong>Usage:</strong>
                   <ol>
                     <li>Select a theme</li>
                     <li>Select the first day the theme will be active</li>
                     <li>Select the last day the theme will be active before switching back to the default theme</li>
                   </ol>
                   The switch will happen automatically every year on the selected dates.'),
  );

  $themes = _theme_autoswitch_getThemes();
  $form['theme'] = array(
    '#type' => 'select',
    '#title' => t('Theme'),
    '#required' => TRUE,
    '#options' => $themes,
    '#default_value' => $record ? $record->theme : NULL,
  );

  $form['date_from_day'] = array(
    '#type' => 'select',
    '#title' => t('Day from'),
    '#required' => TRUE,
    '#options' => _theme_autoswitch_getDays(),
    '#default_value' => $record ? substr($record->date_from, 3, 2) : NULL,
  );
  $form['date_from_month'] = array(
    '#type' => 'select',
    '#title' => t('Month from'),
    '#required' => TRUE,
    '#options' => _theme_autoswitch_getMonths(),
    '#default_value' => $record ? substr($record->date_from, 0, 2) : NULL,
  );

  $form['date_until_day'] = array(
    '#type' => 'select',
    '#title' => t('Day until'),
    '#required' => TRUE,
    '#options' => _theme_autoswitch_getDays(),
    '#default_value' => $record ? substr($record->date_until, 3, 2) : NULL,
  );
  $form['date_until_month'] = array(
    '#type' => 'select',
    '#title' => t('Month until'),
    '#required' => TRUE,
    '#options' => _theme_autoswitch_getMonths(),
    '#default_value' => $record ? substr($record->date_until, 0, 2) : NULL,
  );

  $form['clear'] = array(
    '#type' => 'markup',
    '#value' => '<div class="clear"><!-- clear --></div>',
  );

  $form['#redirect'] = 'admin/settings/theme_autoswitch';
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#suffix' => l(t('Cancel'), 'admin/settings/theme_autoswitch'),
  );

  return $form;
}

/**
 * Merge the rule record with the existing rules (if any) and send them in a single array
 * to the update function.
 *
 * @param $form
 * @param $form_state
 */
function theme_autoswitch_edit_submit($form, &$form_state) {    
  $rule = new stdClass();
  $rule->id         = $form_state['values']['id'] ? $form_state['values']['id'] : NULL;
  $rule->date_from  = $form_state['values']['date_from_month']  . '-' . $form_state['values']['date_from_day'];
  $rule->date_until = $form_state['values']['date_until_month'] . '-' . $form_state['values']['date_until_day'];
  $rule->theme      = $form_state['values']['theme'];

  $rules = _theme_autoswitch_getRules();
  if ($rule->id)
    $rules[$rule->id - 1] = $rule;
  else
    $rules[] = $rule;

  _theme_autoswitch_setRules($rules);
}

/**
 * Generates a confirmation form for deletion of a rule
 *
 * @param $form_state
 * @param $record
 * @return Drupal Form
 */
function theme_autoswitch_confirm_delete($form_state, $record) {
  $form = array();
  $form['id'] = array(
    '#type'  => 'value',
    '#value' => $record->id,
  );
  $form['#redirect'] = 'admin/settings/theme_autoswitch';

  return confirm_form($form, t('Are you sure you want to delete this rule?'), 'admin/settings/theme_autoswitch');
}

/**
 * Deletes the rule from the database
 *
 * @param $form
 * @param $form_state
 */
function theme_autoswitch_confirm_delete_submit($form, $form_state) {
  $rules = _theme_autoswitch_getRules();
  unset($rules[$form_state['values']['id'] - 1]);
  _theme_autoswitch_setRules($rules);
  drupal_set_message('The rule has been deleted');
}

/**
 * Returns the edit links for the rule identified by $id as a string
 *
 * @param integer $id
 * @return string
 */
function _theme_autoswitch_get_edit_links($id) {
  return l(t('Edit'), "admin/settings/theme_autoswitch/$id/edit") . ' | ' . l(t('Delete'), "admin/settings/theme_autoswitch/$id/delete");
}

/**
 * Returns the months of the year in a convenient array for use in the edit form
 *
 * @return array
 */
function _theme_autoswitch_getMonths() {
  return array(
   '01' => t('January'),
   '02' => t('February'),
   '03' => t('March'),
   '04' => t('April'),
   '05' => t('May'),
   '06' => t('June'),
   '07' => t('July'),
   '08' => t('August'),
   '09' => t('September'),
   '10' => t('October'),
   '11' => t('November'),
   '12' => t('December'),
  );
}

/**
 * Returns an array with the days of the month, formatted in the form:
 *
 * array()
 * {
 *   01 => 1,
 *   02 => 2,
 *   ...,
 * }
 *
 * @return array
 */
function _theme_autoswitch_getDays() {
  $days = range(0, 31);
  unset($days[0]);
  foreach ($days as $day) {
    $result[sprintf('%02d', $day)] = $day;
  }
  return $result;
}

/**
 * Returns the given date in the sites default format (this means it includes
 * the current year and a time of 00:00)
 *
 * @param string $date (format: mm-dd)
 * @return string
 */
function _theme_autoswitch_displayDate($date) {
  $months = _theme_autoswitch_getMonths();

  $month = substr($date, 0, 2);
  $day   = substr($date, 3, 2);  

  $date = date_make_date(date('Y') . '-' . $month . '-' . $day);

  if ($date) {
    return date_format_date($date, 'custom', 'F, d');
  }
  else {
    return t('Error loading date');
  }
}